import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChatbotComponent} from './chatbot/chatbot.component';
import {OntologyDetailsComponent} from './ontology-details-component/ontology-details-component.component';

const routes: Routes = [
  {
    path: '',
    component: ChatbotComponent
  },
  {
  path: 'details',
  component: OntologyDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
