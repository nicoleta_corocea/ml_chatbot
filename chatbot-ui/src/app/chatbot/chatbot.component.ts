import {Component, OnInit, ViewChild} from '@angular/core';
import {ChatbotServiceService} from '../shared/chatbot-service.service';
import {FormControl, FormGroup} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {stringify} from 'querystring';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {
  response = '';
  question = '';
  sparqlQuery: string;
  sparqlQueryResponse: string;
  loadingSpinnerAsk = false;
  loadingSpinnerPellet = false;
  loadingSpinnerRDFLib = false;

  constructor(private chatbotService: ChatbotServiceService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    // this.askQuestionForm = new FormGroup({
    //   question: new FormControl()
    // });
  }

  translate() {
    this.loadingSpinnerAsk = true;
    this.chatbotService.translateQuery(this.question).subscribe(data => {
        setTimeout(() => this.response = data);
        this.loadingSpinnerAsk = false;
      },
      err => {
        this.openSnackBar(JSON.parse(err.error).message, 'Close');
        this.response = '';
        this.loadingSpinnerAsk = false;
      });
  }

  runQueryPellet(sparqlQuery) {
    this.loadingSpinnerPellet = true;
    if (sparqlQuery.length === 0) {
      this.openSnackBar('The query cannot be empty', 'Close');
      this.loadingSpinnerPellet = false;
      return;
    }

    this.chatbotService.runQuery(sparqlQuery).subscribe(
      data => {
        this.sparqlQueryResponse = data;
        this.loadingSpinnerPellet = false;
      },
      err => {
        this.openSnackBar(JSON.parse(err.error).message, 'Close');
        this.sparqlQueryResponse = '';
        this.loadingSpinnerPellet = false;
      });
  }

  runQueryRDF(sparqlQuery) {
    this.loadingSpinnerRDFLib = true;
    this.chatbotService.runQueryRDFLib(sparqlQuery).subscribe(data => {
        this.sparqlQueryResponse = data;
        this.loadingSpinnerRDFLib = false;
      },
      err => {
        this.openSnackBar(JSON.parse(err.error).message, 'Close');
        this.sparqlQueryResponse = '';
        this.loadingSpinnerRDFLib = false;
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000,
    });
  }

}
