import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatbotServiceService {

  constructor(private http: HttpClient) { }

  public translateQuery(question: string): Observable<any> {
    return this.http.post('http://localhost:8080/translateQuery', {question}, {responseType: 'text'});

  }

  public runQuery(sparqlQuery: string): Observable<any> {
    // return this.http.post('http://localhost:8080/runQueryWithPellet', {sparqlQuery}, {responseType: 'text'});
    return this.http.post('http://localhost:8080/runQueryWithPellet', {sparqlQuery} , {responseType: 'text'});

  }

  public getClasses(): Observable<any> {
    return  this.http.get('http://localhost:8080/classes', {responseType: 'text'});
  }

  public getIndviduals(): Observable<any> {
    return  this.http.get('http://localhost:8080/individuals',  {responseType: 'text'});
  }

  public getProperties(): Observable<any> {
    return  this.http.get('http://localhost:8080/properties',  {responseType: 'text'});
  }

  public runQueryRDFLib(sparqlQuery: string): Observable<any> {
    return this.http.post('http://localhost:8080/runQueryWithRDFlib', {sparqlQuery}, {responseType: 'text'});

  }
}
