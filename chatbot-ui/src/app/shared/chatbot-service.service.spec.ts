import { TestBed } from '@angular/core/testing';

import { ChatbotServiceService } from './chatbot-service.service';

describe('ChatbotServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChatbotServiceService = TestBed.get(ChatbotServiceService);
    expect(service).toBeTruthy();
  });
});
