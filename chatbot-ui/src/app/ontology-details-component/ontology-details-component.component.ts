import { Component, OnInit } from '@angular/core';
import {ChatbotServiceService} from '../shared/chatbot-service.service';

@Component({
  selector: 'app-ontology-details-component',
  templateUrl: './ontology-details-component.component.html',
  styleUrls: ['./ontology-details-component.component.css']
})
export class OntologyDetailsComponent implements OnInit {
  response: string;
  constructor(private chatbotService: ChatbotServiceService) { }

  ngOnInit() {
  }


  getIndividuals() {
    this.chatbotService.getIndviduals().subscribe(data => {
        this.response = data;
      },
      error => {
        console.log(error);
      });
  }

  getClasses() {
    this.chatbotService.getClasses().subscribe(data => {
      this.response = data;
    });
  }

  getProperties() {
    this.chatbotService.getProperties().subscribe(data => {
      this.response = data;
    });
  }
}
