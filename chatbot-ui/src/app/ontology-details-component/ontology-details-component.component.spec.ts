import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OntologyDetailsComponentComponent } from './ontology-details-component.component';

describe('OntologyDetailsComponentComponent', () => {
  let component: OntologyDetailsComponentComponent;
  let fixture: ComponentFixture<OntologyDetailsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OntologyDetailsComponentComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OntologyDetailsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
