package com.endpoints;

import com.config.BusinessException;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.model.Question;
import com.model.SparqlQuery;
import com.service.PelletService;
import com.service.TranslatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


@RestController
@RequestMapping("")
@CrossOrigin(origins = "http://localhost:4200")
public class ExplainableController {

    @Autowired
    private TranslatorService translatorService;
    @Autowired
    private PelletService pelletService;

    @PostMapping("/translateQuery")
    public String translate(@RequestBody Question question) {
        String query = "";
        if (question.getQuestion().contains("compare")) {
            query = compareRequest(question);

        } else {
            query = translatorService.runPythonCommand(question.getQuestion(), "main.py");
        }
        System.out.println(query);

        if (query.isEmpty() || query.contains("None")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Could not translate question to SPARQL!", new BusinessException());
        }

        return query;
    }

    @PostMapping("/runQueryWithPellet")
    public String runQuery(@RequestBody SparqlQuery sparqlQuery) {
        if (sparqlQuery.getSparqlQuery() == null || sparqlQuery.getSparqlQuery().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SPARQL query should not be empty!", new BusinessException());
        }
        if(sparqlQuery.getSparqlQuery().contains("UPDATE") || sparqlQuery.getSparqlQuery().contains("DELETE")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SPARQL query not permited!", new BusinessException());
        }

        String queryResponse = pelletService.runUserQuery(sparqlQuery);

        return queryResponse;

    }

    @PostMapping("/runQueryWithRDFlib")
    public String runQueryRDFlib(@RequestBody SparqlQuery sparqlQuery) {
        if (sparqlQuery.getSparqlQuery() == null || sparqlQuery.getSparqlQuery().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SPARQL query should not be empty!", new BusinessException());
        }
        if(sparqlQuery.getSparqlQuery().contains("UPDATE") || sparqlQuery.getSparqlQuery().contains("DELETE")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "SPARQL query not permited!", new BusinessException());
        }

        System.out.println(sparqlQuery);
        String queryResponse = translatorService.runPythonCommand(sparqlQuery.getSparqlQuery(), "RDFlib_query_runner.py");

        if(!queryResponse.contains("[")){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Something went wrong and your query could not be processed!", new BusinessException());
        }

        return queryResponse;
    }

    @GetMapping("/individuals")
    public String getIndividuals() {
        String queryResponse = pelletService.runLocalQuery("individuals_query.sparql");

        return queryResponse;
    }

    @GetMapping("/properties")
    public String getProperties() {
        String queryResponse = pelletService.runLocalQuery("properties_query.sparql");

        return queryResponse;
    }

    @GetMapping("/classes")
    public String getClasses() {
        String queryResponse = pelletService.runLocalQuery("classes_query.sparql");

        return queryResponse;
    }

    private String compareRequest(Question question) {
        String query = "";
        if (question.getQuestion().contains("by learning method")) {
            try {
                query = new String(Files.readAllBytes(Paths.get("compare_learning_methods.sparql")));
                String[] words = question.getQuestion().split(" ");
                query = query.replace("A1", words[1]);
                query = query.replace("A2", words[3]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (question.getQuestion().contains("by solved problem")) {
            try {
                query = new String(Files.readAllBytes(Paths.get("compare_learning_problems.sparql")));
                String[] words = question.getQuestion().split(" ");
                query = query.replace("A1", words[1]);
                query = query.replace("A2", words[3]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (question.getQuestion().contains("by algorithm parameters")) {
            try {
                query = new String(Files.readAllBytes(Paths.get("compare_properties.sparql")));
                String[] words = question.getQuestion().split(" ");
                query = query.replace("A1", words[1]);
                query = query.replace("A2", words[3]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return query;
    }

}





