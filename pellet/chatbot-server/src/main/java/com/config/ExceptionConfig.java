package com.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionConfig extends  ResponseEntityExceptionHandler{

    @ExceptionHandler(value = {BusinessException.class})
    public ResponseEntity parseBusinessException(BusinessException ex) {
        Map<String, Object> responseMessage = new LinkedHashMap<>();
        responseMessage.put("status", ex.getStatus());
        responseMessage.put("message", ex.getMessage());

        return new ResponseEntity(responseMessage, HttpStatus.valueOf(ex.getStatus()));
    }


//    @ExceptionHandler(BusinessException.class)
//    public void springHandleNotFound(HttpServletResponse response) throws IOException {
//        response.sendError(HttpStatus.NOT_FOUND.value());
//    }


}

