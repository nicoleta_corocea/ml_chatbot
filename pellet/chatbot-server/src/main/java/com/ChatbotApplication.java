package com;

import com.endpoints.ExplainableController;
import com.model.Ontology;
import com.service.TranslatorService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication(scanBasePackages = "chatbot")
@ComponentScan(basePackageClasses = {ExplainableController.class, TranslatorService.class, Ontology.class})
public class ChatbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatbotApplication.class, args);
	}


}
