package com.model;

public class SparqlQuery {

    private String sparqlQuery;

    public SparqlQuery() {
    }

    public SparqlQuery(String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    public String getSparqlQuery() {
        return sparqlQuery;
    }

    public void setSparqlQuery(String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }
}
