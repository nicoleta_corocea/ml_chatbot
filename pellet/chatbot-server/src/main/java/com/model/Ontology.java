package com.model;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.mindswap.pellet.jena.PelletReasonerFactory;
import org.springframework.stereotype.Component;

@Component("ontology")
public class Ontology {

    private static final String ontologyPath = "../ontologies/machine-learning-ontology.owl";
    private OntModel ontology;

    public Ontology() {
        ontology= ModelFactory.createOntologyModel( PelletReasonerFactory.THE_SPEC );
        ontology.read(ontologyPath);
    }

    public OntModel getOntology() {
        return ontology;
    }
}
