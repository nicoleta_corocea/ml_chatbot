package com.service;

import com.clarkparsia.pellet.sparqldl.jena.SparqlDLExecutionFactory;
import com.config.BusinessException;
import com.hp.hpl.jena.query.*;
import com.model.Ontology;
import com.model.SparqlQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

@Component("pelletService")
public class PelletService {

    @Autowired
    private Ontology ontology;


    public String runLocalQuery(String queryName) {
        Query q = QueryFactory.read(queryName);
        return runQuery(q);
    }

    public String runUserQuery(SparqlQuery sparqlQuery) {
        Query q;
        try {
            q = QueryFactory.create(sparqlQuery.getSparqlQuery());
        } catch (QueryParseException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This is not a valid SPARQL query!", new BusinessException());
        }

        return runQuery(q);

    }

    private String runQuery(Query q) {

        QueryExecution qe = SparqlDLExecutionFactory.create(q, ontology.getOntology());

        ByteArrayOutputStream catcher = new ByteArrayOutputStream();
        PrintStream PS = new PrintStream(catcher);
        PrintStream old = System.out;
        System.setOut(PS);

        ResultSet rs = qe.execSelect();
        ResultSetFormatter.outputAsJSON(rs);

        System.setOut(old);

        String warning;
        String result = catcher.toString();
        if (result.contains("WARN")) {
            String[] results = result.split("\n", 2);
            warning = results[0];
            String[] splitString = warning.split("\\[");
            warning = splitString[splitString.length - 1];
            warning = warning.substring(warning.indexOf(' ') + 1);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, cleanResult(warning), new BusinessException());
        }

        System.out.println("----------------------------------------------------------");
        System.out.println();

        result = cleanResult(result);
        return result;

    }

    private String cleanResult(String result) {
        String substring1 = "http://www.semanticweb.org/machine-learning-ontology#";
        String substring2 = "http://www.w3.org/2002/07/owl#";
        String substring3 = "\"type\": \"uri\" , ";

        result = result.replace(substring1, "");
        result = result.replace(substring2, "");
        result = result.replace(substring3, "");

        return result;
    }


}
