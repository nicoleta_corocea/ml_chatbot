package com.service;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component("quepyService")
public class TranslatorService {

    public String runPythonCommand(String stringArgument, String scriptToRun) {

        String pythonScriptPath = "../translator/" + scriptToRun;
        String[] cmd = new String[3];
        cmd[0] = "./../translator/venv/bin/python";
        cmd[1] = pythonScriptPath;
        cmd[2] = stringArgument;

        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        try {
            pr = rt.exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
        String line = "";
        String queryResponse = "";
        while (true) {
            try {
                if (!((line = bfr.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            queryResponse = queryResponse + line + System.lineSeparator();
        }
        return queryResponse;
    }
}
