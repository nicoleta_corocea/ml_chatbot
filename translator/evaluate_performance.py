# /home/nico/Documents/CHATBOT/code/chatbot/translator/venv/bin/python need !
# coding: utf-8

"""
Main script for translator quepy.
"""
import time

import quepy
from rdflib import Graph
from rdflib.extras.external_graph_libs import rdflib_to_networkx_multidigraph
import networkx as nx
import matplotlib.pyplot as plt
import sys
import ast

# questions = ["class of max_depth"]
# questions = ["compare sarsa and labelPropagation by learning method"]
questions = ["list Algorithm individuals",
             "list DecisionTree individuals",
             "sarsa learning methods",
             "What are the advantages of kMeans?",
             "show details of identity",
             "Data subclass",
             "class of mlpRegressor",
             "algorithm that solve classification problem and have supervised learning method and have feature",
             "algorithms that do not have supervised learning method",
             "algorithm suitable for nonLinearModels or shortDocuments",
             ]

questionss = ["list Algorithm individuals"]
no_of_bad_queries = 0;
bad = []

f = open("eval.txt", "a")
###########################################
print >> f, "BASE TIME"
start = int(time.time() * 1000)
translator = quepy.install("translator")

end = int(round(time.time() * 1000))
quepy_time = " Quepy initialization {}".format(end - start)
#print >> f, quepy_time
print quepy_time

start = int(round(time.time() * 1000))
g = Graph()
result = g.parse(r'/home/nico/Documents/CHATBOT/code/chatbot/ontologies/machine-learning-ontology.owl',
                 format="xml")

rdflib_time = " RDFLib graph build {}".format(end - start)
#print >> f, rdflib_time
print rdflib_time

mean_quepy = [0 for x in range(10)]
mean_rdflib = [0 for x in range(10)]
for i in range(100):
    print i
    for nlp_question in questions:
        #print >> f, " "
        #print >> f, nlp_question
        start = int(time.time() * 1000)
        target, query, metadata = translator.get_query(nlp_question)
        #print query

        end = int(round(time.time() * 1000))
        quepy_time = " Quepy time {}".format(end - start)

        mean_quepy[questions.index(nlp_question)] =  mean_quepy[questions.index(nlp_question)] + end - start
        #print >> f, quepy_time

        start = int(round(time.time() * 1000))
        if (query != None):
            qres = g.query(query)
            #for row in qres:
                #print row
            end = int(round(time.time() * 1000))
            rdflib_time = " RDFLib time {}".format(end - start)
           # print >> f, rdflib_time

            mean_rdflib[questions.index(nlp_question)] = mean_rdflib[questions.index(nlp_question)] + end - start
        else:
           # print "No response"
            bad.append(nlp_question)
            no_of_bad_queries = no_of_bad_queries + 1


for i in range(10):
    mean_quepy[i] = mean_quepy[i]
    mean_rdflib[i] = mean_rdflib[i]


print mean_quepy
print mean_rdflib

for i in range(10):
    mean_quepy[i] = mean_quepy[i]/100
    mean_rdflib[i] = mean_rdflib[i]/100


print mean_quepy
print mean_rdflib
print ""
print "No. of bad queries {}".format(no_of_bad_queries)
print bad
