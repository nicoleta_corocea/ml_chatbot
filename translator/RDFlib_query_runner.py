# /home/nico/Documents/CHATBOT/code/chatbot/translator/venv/bin/python need !
# coding: utf-8

"""
Script for running SPARQL query using rdflib
"""
import json, re

from rdflib import Graph
import sys

PREFIXES = ["http://www.semanticweb.org/machine-learning-ontology#",
            "http://www.w3.org/2002/07/owl#"]


def to_json(qres):
    x = []
    for row in qres:
        dictionary_response = {}
        for i in range(len(row)):
            my_res = row[i]
            for prefix in PREFIXES:
                if prefix in row[i]:
                    my_res = re.sub(prefix, "", row[i])
            dictionary_response[row.labels.keys()[row.labels.values().index(i)]] \
                                = my_res
            x.append(dictionary_response)

    y = json.dumps(x, sort_keys=False, indent=4)
    return y


query = sys.argv[1]

f = open("output.txt", "w")
print >> f, query
g = Graph()
g.parse(r'../ontologies/machine-learning-ontology.owl', format="xml")
qres = g.query(query)
y = to_json(qres)
print >> f, y
f.close()
print y
