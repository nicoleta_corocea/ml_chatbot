from refo import Group, Question, Plus
from quepy.dsl import HasKeyword
from quepy.parsing import Lemma, Pos, QuestionTemplate, Lemmas, Particle, Token

from dsl import *

nouns = Plus(Pos("NN") | Pos("NNS") | Pos("NNP") | Pos("NNPS"))


def factory(classname):
    if (classname.endswith("s") and (not classname == "IsGaussianProcess") and (not classname == "IsNaiveBayes")):
        classname = classname[0: len(classname) - 1]
    cls = globals()[classname]
    return cls()


class Algorithm(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsAlgorithm() + HasInstance(name, self.operator)


class LearningMethod(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsLearningMethod(self.operator) + HasInstance(name, self.operator)


class LearningProblem(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsLearningProblem() + HasInstance(name, self.operator)


class Feature(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR") | Plus(
        Pos("JJ") | Pos("NN"))

    def interpret(self, match):
        name = match.words.tokens
        return IsFeature() + HasInstance(name, self.operator)


class AlgorithmAdvantage(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsAlgorithmAdvantage() + HasInstance(name, self.operator)


class AlgorithmDisadvantage(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsAlgorithmDisadvantage() + HasInstance(name, self.operator)


class Data(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsData() + HasInstance(name, self.operator)


class DataCharacteristic(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsDataCharacteristic() + HasInstance(name, self.operator)


class Parameter(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR") | Plus(
        Pos("JJ") | Pos("NN"))

    def interpret(self, match):
        name = match.words.tokens
        return IsParameter() + HasInstance(name, self.operator)


class PerformanceMetric(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsPerformanceMetric() + HasInstance(name, self.operator)


class Provenance(Particle):
    regex = Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("JJ") | Pos("VBN") | Pos("JJR")

    def interpret(self, match):
        name = match.words.tokens
        return IsProvenance() + HasInstance(name, self.operator)


"""
-----------------------------------------------------------------------
"""


class ListIndividuals(QuestionTemplate):
    """
    Regex for printing individuals of given class
    Ex: "What are the instances of Feature?"/individuals
        "individuals of Feature"/instances
        "print Feature individuals"/instances
        "show Feature individuals"/instances
        "list Algorithm"

    """
    target = Group(Pos("NN") | Pos("NNP") | Pos("NNS"), "target")
    optional_opening = Question((Pos("WP") | Pos("WDT"))
                                + Lemma("be") + Pos("DT"))
    regex1 = optional_opening \
             + Question(Lemma("individual") | Lemma("instance")) + \
             Pos("IN") + target + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) \
             + target + \
             Question(Lemma("individual") | Lemma("instance"))

    regex = (regex1 | regex2)

    def interpret(self, match):
        algorithms = factory("Is" + match.target.tokens)
        return algorithms, "enum"


"""
============================ PROPERTIES===========================
"""
"""
--------------has-learning-method 
"""


class GivenAlgorithmHasLearningMethod(QuestionTemplate):
    """
    learning methods of given algorithm
    "What are the learning method/s of A1?"
    "list A1 learning methods"/print/show
    "Clustering learning methods"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("learn") + Lemma("method")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("learn") + (Lemma("method") | Lemma("methods")))

    regex3 = Algorithm() + (Lemma("learn") + (Lemma("method") | Lemma("methods")))
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        alg = HasLearningMethodReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-advantage
"""


class AlgorithmHasAdvantage(QuestionTemplate):
    """
    advantages of given algorithm
    "What are the advantages of A1?"
    "list A1 advantages"/print/show
    "Clustering advantages"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("advantage") | Lemma("advantages")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("advantage") | Lemma("advantages"))

    regex3 = Algorithm() + (Lemma("advantage") | Lemma("advantages"))
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        alg = HasAdvantageReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-disadvantage
"""


class AlgorithmHasDisadvantage(QuestionTemplate):
    """
    disadvantages of given algorithm
    "What are the disadvantages of A1?"
    "list A1 disadvantages"/print/show
    "Clustering disadvantages"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("disadvantage") | Lemma("disadvantages")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("disadvantage") | Lemma("disadvantages"))

    regex = (regex1 | regex2)

    def interpret(self, match):
        alg = HasDisadvantageReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-feature-characeristic
"""


class FeatureCharacteristic(QuestionTemplate):
    """
    feature characteristics of given algorithm
    "What are the feature characteristic/s of A1?"
    "list A1 feature characteristics"/print/show
    "Clustering learning methods"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("feature") + Lemma("characteristic") | Lemma("characteristics")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("feature") + Lemma("characteristic") | Lemma("characteristics"))

    regex3 = Algorithm() + (Lemma("feature") + Lemma("characteristic") | Lemma("characteristics"))
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        alg = HasFeatureReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-parameter
"""


class AlgorithmParameters(QuestionTemplate):
    """
    parameters of given algorithm
    "What are the parameters of A1?"
    "list A1 parameters"/print/show
    "Clustering parameters"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("parameter") | Lemma("parameters")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("parameter") | Lemma("parameters"))

    regex3 = Algorithm() + (Lemma("parameter") | Lemma("parameters"))
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        alg = HasParameterReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-performance-result
"""


class AlgorithmPerformanceResult(QuestionTemplate):
    """
    performance results of given algorithm
    "What are the performance result/s of A1?"
    "list A1 performance result"/print/show
    "Clustering performance result"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + (Lemma("performance") + Lemma("result") | Lemma("results")) + \
             Pos("IN") + Algorithm() + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + Algorithm() + \
             (Lemma("performance") + Lemma("result") | Lemma("results"))

    regex3 = Algorithm() + (Lemma("performance") + Lemma("result") | Lemma("results"))
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        alg = HasPerformanceResultReversed(match.algorithm)
        return alg, "enum"


"""
---------------has-provenance
"""


class IndividualProvenance(QuestionTemplate):
    """
    igb provenance
    """

    target = Group(
        Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("FW") | Pos("JJR") | Plus(Pos("JJ") | Pos("NN")),
        "target")

    regex = target + Lemma("provenance")

    def interpret(self, match):
        target = HasInstance(match.target.tokens, operator="")
        subclasses = HasProvenanceReversed(target)

        return subclasses, "enum"


"""
---------------solves
"""


class AlgorithmSolvesProblem(QuestionTemplate):
    """
    algorithm solves problem
    "What learning problem does A1 solve?"
    "list problems solved by A1"
    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("learn") + Lemma("problem") + Lemma("do"))
    regex1 = optional_opening + Algorithm() + Lemma("solve") + Question(Pos("."))

    regex2 = Question(Lemma("list") | Lemma("show") | Lemma("print")) + (Lemma("problem") | Lemma("problems")) + Lemma(
        "solve") + Pos("IN") + Algorithm()

    regex = (regex1 | regex2)

    def interpret(self, match):
        alg = SolvesLearningProblemReversed(match.algorithm)
        return alg, "enum"


"""
---------------suitable-for
"""


class AlgorithmSuitableFor(QuestionTemplate):
    """

    what data is A1 algorithm suitable for?

    """
    optional_opening = Question((Pos("WP") | Pos("WDT")) + Lemma("data") + Lemma("be"))
    regex = Algorithm() + Lemma("algorithm") + Lemma("suitable") + Pos("IN")


"""
============================ SUBCLASS OF ==========================
"""


class MySubclassOf(QuestionTemplate):
    """
    <Class name> subclass
    """
    target = Group(Pos("NN") | Pos("NNP"), "target")
    optional_opening = Question(Pos("WP") + Lemma("be") + Pos("DT"))
    regex1 = optional_opening + Question(Lemma("subclass") | Lemma("family")) + \
             Pos("IN") + target + Question(Pos("."))
    regex3 = Question(Lemma("method")) + Question(Pos("IN")) + target

    regex2 = target + Question(Lemma("subclass"))

    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        target = HasInstance(match.target.tokens, operator="")
        subclasses = IsSubclassOf(target)

        return subclasses, "enum"


"""
-------NOT------------- algorithms that does not have M1 learning method
"""


class NotHaveLearningMethod(QuestionTemplate):
    """
    algorithms that does not have <> learning method
    "algorithm that do not have M1"
    "algorithms that do not use M1"
    "algorithm that does not have M1"
    "algorithms that does not have M1"
    "algorithms that does not have M1 learning method"
    """
    regex2 = Question(Lemmas("algorithm that do not") + (Lemma("have"))) + \
             LearningMethod(operator="MINUS ") + Question(Lemmas("learn method"))
    regex = (regex2)

    def interpret(self, match):
        alg = IsAlgorithm()
        asdasd = NotHasLearningMethod(match.learningmethod)
        return alg + asdasd, "enum"


"""
---------------- and ---- scalabil
"""


class SolvesProblemAndHasLearningMethod(QuestionTemplate):
    """ algorithms that solve Clustering-problem  and have learning method Unsupervised"""

    # regex = Lemma("algorithm")+ Lemma("that") + Lemma("solve") + target1 + Lemma("and") + Lemma("have") + Lemmas("learning method") + target2
    regex1 = Question(Lemmas("algorithm that")) + Question((Lemma("solve") | Lemma("resolve"))) + \
             LearningProblem(operator="AND") + Question(Lemma("problem")) + \
             Question(Lemma("and")) + Question(Lemma("use") | Lemma("have")) + LearningMethod(operator="AND") + \
             Question(Lemmas("learning method")) + \
             Question(Lemma("and")) + Question(Lemma("have")) + Question(Lemma("feature")) + Feature(operator="AND")

    # regex = LearningProblem(operator="AND") + LearningMethod(operator="AND") + Feature(operator="AND")
    regex = regex1

    def interpret(self, match):
        algorithms = IsAlgorithm()
        learning_methods = HasLearningMethod(match.learningmethod)
        learning_problems = SolvesLearningProblem(match.learningproblem)
        features = HasFeature(match.feature)
        return algorithms + learning_methods + learning_problems + features, "enum"


"""
--------------- or
"""


class HasFeature1OrFeature2(QuestionTemplate):
    # regex = Feature(operator="OR")  + Lemma("or") + Feature(name = "second_feature", operator = "OR")
    regex1 = Question(Lemmas("algorithm that")) + Question(Lemma("have")) + \
             Question(Lemma("feature")) + Feature(operator="OR") + \
             Lemma("or") + Question(Lemma("have")) + Feature(operator="OR", name="feature2")

    regex = regex1

    # regex = Feature(operator="OR") + Lemma("or") + LearningMethod(operator="OR")

    def interpret(self, match):
        algorithms = IsAlgorithm()
        # feature = IsFeature()

        feature2 = HasFeature(match.feature)
        learingmethod = HasFeature(match.feature2)

        return algorithms + feature2 + learingmethod, "enum"


class AlgorithmSuitableForD1orD2(QuestionTemplate):
    """

    algorithms suitable for D1 or D2 data characteristic/s
    """

    regex = Lemma("algorithm") + Lemma("suitable") + Pos("IN") + DataCharacteristic(
        name="dc1", operator="OR") + Lemma(
        "or") + DataCharacteristic(name="dc2", operator="OR") + Question(Lemmas("data characteristic"))

    def interpret(self, match):
        algorithm = IsAlgorithm()
        data_chr1 = SuitableFor(match.dc1)
        data_chr2 = SuitableFor(match.dc2)

        return algorithm + data_chr1 + data_chr2, "enum"


"""
--------------------------WHAT IS (individual) ---------------
"""


class WhatIs(QuestionTemplate):
    """
    what is A1
    A1 type
    A1 class
    """
    target = Group(
        Pos("NN") | Pos("NNP") | Pos("NNS") | Pos("VBG") | Pos("FW") | Pos("JJR") | Plus(
            Pos("JJ") | Pos("NN")),
        "target")
    regex1 = target + Question(Lemma("type") | Lemma("class"))
    regex2 = Lemma("what") + Lemma("be") + Question(target) + Question(Lemma("type") | Lemma("class"))
    regex3 = Question((Lemma("type") | Lemma("class")) + Pos("IN")) + target
    regex = (regex1 | regex2 | regex3)

    def interpret(self, match):
        target = HasInstance(match.target.tokens, operator="")
        type = HasType(target)

        return type, "enum"


"""
------------------------------- comment
"""


class IndividualComment(QuestionTemplate):
    target = Group(Pos("NN") | Pos("NNP") | Pos("JJ"), "target")
    intro = Lemma("show") | Lemma("list") | Lemma("print")
    regex1 = intro + Question((Lemma("detail") | Lemma("details")) + Pos("IN")) + target
    regex2 = intro + Question((Lemma("definition") | Lemma("definitions")) + Pos("IN")) + target
    regex3 = Lemma("define") + target + Lemma("individual")
    regex4 = Lemmas("tell me more about") + target
    regex = (regex1 | regex2 | regex3 | regex4)

    def interpret(self, match):
        thing = HasInstance(match.target.tokens, operator="")
        thingy = AnnotatedSource(thing)
        comm = IsDefinedIn(thingy)

        return comm, "enum"
