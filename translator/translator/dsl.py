from copy import copy

from quepy.dsl import FixedRelation, FixedType, HasKeyword, FixedDataRelation
from quepy.encodingpolicy import encoding_flexible_conversion
from quepy.expression import Expression


class MinusFixedRelation(Expression):
    relation = None
    reverse = False

    def __init__(self, destination, reverse=None):
        if reverse is None:
            reverse = self.reverse
        super(MinusFixedRelation, self).__init__()
        if self.relation is None:
            raise ValueError("You *must* define the `relation` "
                             "class attribute to use this class.")
        self.relation = "MINUS " + self.relation
        self.nodes = copy(destination.nodes)
        self.head = destination.head
        self.decapitate(self.relation, reverse)


class FixedInstance(Expression):
    relation = None

    def __init__(self, data, operator):
        super(FixedInstance, self).__init__()
        if self.relation is None:
            raise ValueError("You *must* define the `instance` "
                             "class attribute to use this class.")
        self.relation = encoding_flexible_conversion(operator + self.relation)
        self.add_data(self.relation, data)


class IsDefinedIn(FixedRelation):
    relation = "rdfs:comment"
    reverse = True


"""
-------------------------Algorithm and its subclasses
"""


class IsAlgorithm(FixedType):
    fixedtype = "ml:Algorithm"


class IsNaiveBayes(FixedType):
    fixedtype = "ml:NaiveBayes"


class IsAssociationMining(FixedType):
    fixedtype = "ml:AssociationMining"


class IsClustering(FixedType):
    fixedtype = "ml:Clustering"


class IsDecisionTree(FixedType):
    fixedtype = "ml:DecisionTree"


class IsEnsembleLearning(FixedType):
    fixedtype = "ml:EnsembleLearning"


class IsGaussianProcess(FixedType):
    fixedtype = "ml:GaussianProcess"


class IsNearestNeighbor(FixedType):
    fixedtype = "ml:NearestNeighbor"


class IsLabelPropagation(FixedType):
    fixedtype = "ml:LabelPropagation"


class IsNeuralNetworkModel(FixedType):
    fixedtype = "ml:NeuralNetworkModel"


class IsNeuralNetworkModel(FixedType):
    fixedtype = "ml:NeuralNetworkModel"


class IsSupportVectorMachine(FixedType):
    fixedtype = "ml:SupportVectorMachine"


class IsStochasticGradientDescent(FixedType):
    fixedtype = "ml:StochasticGradientDescent"


"""
----------------------AlgorithmAdvantage and Disadvantage
"""


class IsAlgorithmAdvantage(FixedType):
    fixedtype = "ml:AlgorithmAdvantage"


class IsAlgorithmDisadvantage(FixedType):
    fixedtype = "ml:AlgorithmDisadvantage"


"""
------------------------Data and its subclasses
"""


class IsData(FixedType):
    fixedtype = "ml:Data"


class IsFeature(FixedType):
    fixedtype = "ml:Feature"


class IsDataCharacteristic(FixedType):
    fixedtype = "ml:DataCharacteristic"


"""
------------------------ Learning Method and Learning Problem
"""


class IsLearningMethod(FixedType):
    fixedtype = "ml:LearningMethod"

    def __init__(self, operator):
        super(IsLearningMethod, self).__init__(operator)
        self.fixedtype = operator + "ml:LearningMethod"


class IsLearningProblem(FixedType):
    fixedtype = "ml:LearningProblem"


"""
------------------------- PARAMETER
"""


class IsParameter(FixedType):
    fixedtype = "ml:Parameter"


"""
------------
"""


class IsAssociationParameter(FixedType):
    fixedtype = "ml:AssociationParameter"


class IsIGBParameter(FixedType):
    fixedtype = "ml:IGBParameter"


class IsTopKRulesParameter(FixedType):
    fixedtype = "ml:TopKRulesParameter"


"""
-------------
"""


class IsClusteringParameter(FixedType):
    fixedtype = "ml:ClusteringParameter"


class IsKMeansParameter(FixedType):
    fixedtype = "ml:KMeansParameter"


class IsKMeansAlgorithmParameter(FixedType):
    fixedtype = "ml:KMeansAlgorithmParameter"


class IsAgglomerativeClusteringParameter(FixedType):
    fixedtype = "ml:AgglomerativeClusteringParameter"


class IsLinkageParameter(FixedType):
    fixedtype = "ml:LinkageParameter"


"""
--------------
"""


class IsDTParameter(FixedType):
    fixedtype = "ml:DTParameter"


"""
--------------
"""


class IsEnsembleMethodsParameter(FixedType):
    fixedtype = "ml:EnsembleMethodsParameter"


class IsAdaboostParameter(FixedType):
    fixedtype = "ml:AdaboostParameter"


class IsRandomForestParameter(FixedType):
    fixedtype = "ml:RandomForestParameter"


"""
------------
"""


class IsGaussianProcessesParameter(FixedType):
    fixedtype = "ml:GaussianProcessesParameter"


"""
------------
"""


class IsLabelPropagationParameter(FixedType):
    fixedtype = "ml:LabelPropagationParameter"


"""
------------
"""


class IsNaiveBayesParameter(FixedType):
    fixedtype = "ml:NaiveBayesParameter"


class IsBernoulliNBParameter(FixedType):
    fixedtype = "ml:BernoulliNBParameter"


class IsComplementNBParameter(FixedType):
    fixedtype = "ml:ComplementNBParameter"


class IsGaussianNBParameter(FixedType):
    fixedtype = "ml:GaussianNBParameter"


class IsMultinomialNBParameter(FixedType):
    fixedtype = "ml:MultinomialNBParameter"


"""
------------
"""


class IsNeuralNetworkParameter(FixedType):
    fixedtype = "ml:NeuralNetworkParameter"


class IsNeuralNetworkActivationFunction(FixedType):
    fixedtype = "ml:NeuralNetworkActivationFunction"


class IsNeuralNetworkLearningRate(FixedType):
    fixedtype = "ml:NeuralNetworkLearningRate"


class IsNeuralNetworkSolver(FixedType):
    fixedtype = "ml:NeuralNetworkSolver"


"""
--------------
"""


class IsNNParameter(FixedType):
    fixedtype = "ml:NNParameter"


class IsNNAlgorithm(FixedType):
    fixedtype = "ml:NNAlgorithm"


"""
---------------
"""


class IsReinforcementParameter(FixedType):
    fixedtype = "ml:ReinforcementParameter"


"""
---------------
"""


class IsSGDParameter(FixedType):
    fixedtype = "ml:SGDParameter"


class IsLossParamsForClassification(FixedType):
    fixedtype = "ml:LossParamsForClassification"


class IsLossParamsForRegression(FixedType):
    fixedtype = "ml:LossParamsForRegression"


class IsSGDLearningRate(FixedType):
    fixedtype = "ml:SGDLearningRate"


"""
------------------
"""


class IsSVMParameter(FixedType):
    fixedtype = "ml:SVMParameter"


class IsSVCParameter(FixedType):
    fixedtype = "ml:SVCParameter"


class IsSVRParameter(FixedType):
    fixedtype = "ml:SVRParameter"


"""
------------------------Performance Metric
"""


class IsPerformanceMetric(FixedType):
    fixedtype = "ml:PerformanceMetric"


"""
------------------------Provenance
"""


class IsProvenance(FixedType):
    fixedtype = "ml:Provenance"


"""
-------------------- properties & reversed properties & negate properties
"""


class HasAdvantageReversed(FixedRelation):
    relation = "ml:has-advantage"
    reverse = True


class HasAdvantage(FixedRelation):
    relation = "ml:has-advantage"
    reverse = False


"""
----------
"""


class HasDisadvantageReversed(FixedRelation):
    relation = "ml:has-disadvantage"
    reverse = True


class HasDisadvantage(FixedRelation):
    relation = "ml:has-disadvantage"
    reverse = False


"""
---------
"""


class HasFeature(FixedRelation):
    relation = "ml:has-feature-characteristic"


class HasFeatureReversed(FixedRelation):
    relation = "ml:has-feature-characteristic"
    reverse = True


"""
----------
"""


class HasLearningMethodReversed(FixedRelation):
    relation = "ml:has-learning-method"
    reverse = True


class NotHasLearningMethod(MinusFixedRelation):
    relation = "ml:has-learning-method"
    reverse = False


class HasLearningMethod(FixedRelation):
    relation = "ml:has-learning-method"
    reverse = False


"""
------------
"""


class HasParameter(FixedRelation):
    relation = "ml:has-parameter"
    reverse = False


class HasParameterReversed(FixedRelation):
    relation = "ml:has-parameter"
    reverse = True


"""
------------
"""


class HasPerformanceResult(FixedRelation):
    relation = "ml:has-performance-result"
    reverse = False


class HasPerformanceResultReversed(FixedRelation):
    relation = "ml:has-performance-result"
    reverse = True


"""
----------
"""


class HasProvenance(FixedRelation):
    relation = "ml:has-provenance"
    reverse = False


class HasProvenanceReversed(FixedRelation):
    relation = "ml:has-provenance"
    reverse = True


"""
----------
"""


class SolvesLearningProblem(FixedRelation):
    relation = "ml:solves"
    reverse = False


class SolvesLearningProblemReversed(FixedRelation):
    relation = "ml:solves"
    reverse = True


"""
----------
"""


class SuitableFor(FixedRelation):
    relation = "ml:suitable-for"
    reverse = False


class SuitableForReversed(FixedRelation):
    relation = "ml:suitable-for"
    reverse = True


"""
-----------
"""


class NameOf(FixedRelation):
    relation = "foaf:name"
    # relation = "dbpprop:name"
    reverse = True


class LabelOf(FixedRelation):
    relation = "rdfs:label"
    reverse = True


class IsSubclassOf(FixedRelation):
    relation = "rdfs:subClassOf"
    reverse = False


class HasType(FixedRelation):
    relation = "rdf:type"
    reverse = True


class AnnotatedSource(FixedRelation):
    relation = "owl:annotatedSource"


class HasGivenClass(FixedType):
    fixedtype = ""

    def __init__(self, fixedtype):
        self.fixedtype = "ml:" + fixedtype


class HasGivenRelML(FixedDataRelation):
    relation = ""

    def __init__(self, relation):
        self.relation = "ml:" + relation


class HasInstance(FixedInstance):
    relation = u"FILTER"

    def __init__(self, data, operator):
        super(HasInstance, self).__init__(data, operator)

