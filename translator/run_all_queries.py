# /home/nico/Documents/CHATBOT/code/chatbot/translator/venv/bin/python need !
# coding: utf-8

"""
Main script for translator quepy.
"""

import quepy
from rdflib import Graph
from rdflib.extras.external_graph_libs import rdflib_to_networkx_multidigraph
import networkx as nx
import matplotlib.pyplot as plt
import sys
import ast

translator = quepy.install("translator")
# nlp_question = sys.argv[1]
questions = [
    # 1
    # nlp_question = "list Algorithm" --conflict
    "Which are the instances of Algorithm?",  # singualar/plural
    "individuals of DecisionTree",
    "print NaiveBayes individuals",
    "show LearningProblem instances",
    "list NaiveBayes individuals",

    # 2
    # relation to an instance.
    "sarsa learning methods",
    "What are the learning methods of igb?",
    "print igb learning methods",
    "list topKRules learning methods",
    #"show topKRules learning methods",

    # 3
    "What are the advantages of kMeans?",
    "list agglomerativeClustering advantages",
    "decisionTreeClassifier advantages",

    # 4
    # disadvantages of given algorithm
    "What are the disadvantages of supportVectorClassification?",
    "list bernoulliNaiveBayes disadvantages",
    "print supportVectorClassification disadvantages",
    "print linearSVC disadvantages",
    #"show linearSVC disadvantages",
    "linearSVC disadvantages",

    # 5
    # performance results of given algorithm
    "What are the performance results of qlearn?",
    "list linearSVR performance results",
    #"show linearSVR performance result",
    "print linearSVR performance results",
    "sarsa performance result",

    # 6
    # parameters of given algorithm
    "What are the parameters of bernoulliNaiveBayes?",
    "list linearSVC parameters",
    "print linearSVR parameters",
    "supportVectorClassification parameters",

    # 7
    # feature characteristics of given algorithm
    "What are the feature characteristics of linearSVC?",
    "list decisionTreeClassifier feature characteristic",
    "topKRules feature characteristics",

    # 8
    "igb provenance",

    # 9
    # algorithm solves problem
    "What learning problem does topKRules solve?",
    "list problems solved by kMeans",

    # 10
    # DE CE MERGE CU <SUBCLASS> SI CU <FAMILY> NU??????
    "Data subclass",
    # "method of Data", #bad
    # "Data family",  #bad
    "What are the subclass of Data?",

    # 11
    # what is <name>
    "what is max_depth",  # + class/type
    "sgdClassifier type",
    "sgdClassifier class",
    "type of nearestNeighbor",
    "class of mlpRegressor",

    # ------------ OPERATORI -------------

    # 12 NOT
    # algorithms that does not have M1 learning method
    "algorithm that does not have supervised",
    "algorithms that do not have supervised learning method",
    # have/use; learning method is optional; algorithm/s do/does

    # 13 AND
    # "P1 M1 F1", #algorithms that solve P1 problem and have M1 learning method and have feature F1
    # "association supervised binaryOrBoolean", #algorithms that solve P1 problem and have M1 learning method and have feature F1
    "algorithm that solve classification problem and have supervised learning method and have feature complexInteractions",
    "algorithm that solve classification problem and have supervised and have feature complexInteractions",
    "algorithm that solve P1 problem and have M1 and have F1",
    "algorithm that solve P1 and have M1 and have discreteFeature",

    # 14 OR

    "algorithm that have F1 or F2",  # algorithms that have feature P1 OR P2 problem
    "algorithm suitable for nonLinearModels or shortDocuments",
    # 15
    # rdf:comment for individual
    "show details of identity",
    "list definitions of identity",
    "print definition of identity",
    "define identity individual",
    "tell me more about normalize_y"
]
# questions = ["class of max_depth"]
# questions = ["compare sarsa and labelPropagation by learning method"]
#questions = ["algorithm that does not have supervised"]

g = Graph()
result = g.parse(r'/home/nico/Documents/CHATBOT/code/chatbot/ontologies/machine-learning-ontology.owl', format="xml")
no_of_bad_queries = 0;
bad = []
for nlp_question in questions:
    target, query, metadata = translator.get_query(nlp_question)
    print ""
    print "--------------------------------------------------------------------------------------------------"
    print ""
    print nlp_question
    print ""
    print query

    if (query != None):
        qres = g.query(query)
        for row in qres:
            print row
    else:
        print "No response"
        bad.append(nlp_question)
        no_of_bad_queries = no_of_bad_queries + 1

print ""
print "No. of bad queries {}".format(no_of_bad_queries)
print bad
