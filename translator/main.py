#/home/nico/Documents/CHATBOT/code/chatbot/translator/venv/bin/python need !
# coding: utf-8

"""
Main script for translator quepy.
"""

import quepy
from rdflib import Graph
import sys
import ast

translator = quepy.install("translator")
nlp_question = sys.argv[1]

#1
#nlp_question = "list Algorithm" --conflict
#nlp_question = "What are the instances of Feature?"
#nlp_question = "individuals of Feature"
#nlp_question = "print NaiveBayes individuals"
#nlp_question = "show LearningProblem instances"
#nlp_question = "list Feature individuals"

#2
#nlp_question = "Clustering learning methods" #relation to an instance.
#nlp_question = "What are the learning methods of A1?"
#nlp_question = "print A1 learning methods"
#nlp_question = "list A1 learning methods"
#nlp_question = "show A1 learning methods"


#3
# DE CE MERGE CU <SUBCLASS> SI CU <FAMILY> NU??????
#nlp_question = "Data subclass"

#nlp_question = "method of Data"

#nlp_question = "Data family"
#nlp_question = "What are the subclass of Data?"

#4
# what is <name>
#nlp_question = "what is M1" # + class/type
#nlp_question = "A1 type"
#nlp_question = "A1 class"
#nlp_question = "type of A1"
#nlp_question = "class of A1"

"""
---- operatori ----
"""
#5 NOT
#algorithms that does not have M1 learning method
#nlp_question = "algorithm that do not have M1"
#nlp_question = "algorithms that does not use M1 learning method" #have/use; learning method is optional; algorithm/s do/does


#6 AND
#nlp_question = "P1 M1 F1" #algorithms that solve P1 problem and have M1 learning method and have feature F1
#nlp_question = "algorithm that solve P1 problem and have M1 learning method and have feature F1"
#nlp_question = "algorithm that solve P1 problem and have M1 and have feature F1"
#nlp_question = "algorithm that solve P1 problem and have M1 and have F1"
#nlp_question = "algorithm that solve P1 and have M1 and have F1"
#nlp_question = "solve P1 and have M1 and have F1"
#nlp_question = "solve P1 and have M1 and have feature F1"


#7 OR

#nlp_question = "F1 or F2" #algorithms that have feature P1 OR P2 problem


#8
# rdf:comment for individual
#nlp_question = "show details of identity"
#nlp_question = "list definitions of identity"
#nlp_question = "print definition of identity"
#nlp_question = "define identity individual"

"""
---- conflicte ----
"""




"""
---- unresolved ----
"""
#nlp_question = "instances solves"
#nlp_question = "unsupervised learning method algorithms"
#nlp_question = "list learning methods"

target, query, metadata = translator.get_query(nlp_question)
#print query
print query
query = "PREFIX  owl:  <http://www.w3.org/2002/07/owl#> PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX  quepy:" + \
        "<http://www.machinalis.com/quepy#> PREFIX  skos: <http://www.w3.org/2004/02/skos/core#> PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#> " + \
        "PREFIX  foaf: <http://xmlns.com/foaf/0.1/>PREFIX  ml:   <http://www.semanticweb.org/machine-learning-ontology#> " + \
        "SELECT DISTINCT  ?x0 ?p ?v WHERE { ?x0 rdfs:comment ?x1 .?x0 ?p ?v.}"

prefix = "PREFIX  owl:  <http://www.w3.org/2002/07/owl#> PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX  quepy:" + \
         "<http://www.machinalis.com/quepy#> PREFIX  skos: <http://www.w3.org/2004/02/skos/core#> PREFIX  rdfs: <http://www.w3.org/2000/01/rdf-schema#> " + \
         "PREFIX  foaf: <http://xmlns.com/foaf/0.1/>PREFIX  ml:   <http://www.semanticweb.org/machine-learning-ontology#> "
query = prefix + " SELECT DISTINCT  ?x0 WHERE { "  + \
        "?x0 rdf:type ml:DecisionTree.}"



"""
g = Graph()
g.parse(r'/home/nico/Documents/CHATBOT/code/chatbot/ontologies/machine-learning-ontology.owl',format="xml")
qres = g.query(query)
for row in qres:
    print row
"""


